import path from 'path'
import express from 'express'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import cors from 'cors'
import dotenv from 'dotenv'
import cardStats from './routes/cardStats.js'
import stats from './routes/cardStats.js'
import colors from 'colors'
import connectDB from './config/db.js'

dotenv.config()

connectDB()

const PORT = process.env.PORT || 8080

const app = express()

app.use(
  cors({
    origin: 'http://localhost:5000' || 'http://localhost:8080',
  })
)

app.use(bodyParser.json())

//Import Routes (Middleware)

app.use('/cardStats', cardStats)

// Routes
// app.get('/', (req, res) => {
//   res.send('Hello traveler')
// })

// Use Routes
app.use('./cardStats', cardStats)

const __dirname = path.resolve()
if (process.env.NODE_ENV) {
  app.use(express.static(path.join(__dirname, '/client/build')))

  app.get('*', (req, res) =>
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
  )
} else {
  app.get('/', (req, res) => {
    res.send('API is running.....')
  })
}

app.listen(PORT, () => console.log('App is running'))
