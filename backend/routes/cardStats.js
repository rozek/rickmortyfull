import express from 'express'
const router = express.Router()
import CardStats from '../models/cardStats.js'

// ROUTES
router.get('/', async (req, res) => {
  try {
    const stats = await CardStats.find()
    res.json(stats)
  } catch (err) {
    res.json({ message: err })
  }
})

router.get('/userStats', async (req, res) => {
  console.log(res)
  try {
    const stats = await CardStats.find()
    res.json(stats)
  } catch (err) {
    res.json({ message: err })
  }
})

router.post('/userStats', async (req, res) => {
  // new CardStats tworzymy na podstawie modelu zaimportowanego z Post
  const cardStats = new CardStats({
    title: req.body.title,
    description: req.body.description,
  })
  try {
    const savedStats = await cardStats.save()
    res.json(savedStats)
  } catch (err) {
    res.json({ message: err })
  }
})

// Update state

router.post('/', async (req, res) => {
  try {
    const updateStats = await CardStats.update({
      title: req.body.title,
      description: req.body.description,
    })
    res.json(updateStats)
  } catch (err) {
    res.json({ message: err })
  }
})

// Create State

// router.post('/', async (req,res) => {
// // new CardStats tworzymy na podstawie modelu zaimportowanego z Post
//     const cardStats = new CardStats({
//         title: req.body.title,
//         description: req.body.description
//     })
//     try {
//         const savedStats = await cardStats.save()
//         res.json(savedStats)
//     }catch(err) {
//         res.json({message: err})
//     }

// })

export default router
