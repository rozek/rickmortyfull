import mongoose from 'mongoose'

const PostSchema = mongoose.Schema({
  title: {
    type: Array,
    required: true,
  },
  description: {
    type: Array,
    required: true,
  },
  user: {
    type: String,
    required: true,
    default: 'anonimus',
  },
  date: {
    type: Date,
    default: Date.now,
  },
})

const cardStats = mongoose.model('cardStats', PostSchema)

export default cardStats
