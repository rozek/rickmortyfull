import React, { useEffect, useState, useRef } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import ReactDOM from 'react-dom'
import Nav from './components/navigation/Nav'
import Header from './components/ui/Header'
import About from './components/about/About'
import Characters from './components/characters/Characters'
import CharacterSpecification from './components/characters/CharacterSpecification'
import HandleReducer from './components/tools/HandleReducer'
// import Statistic from './components/statistics/Statistic'
import Statistic from './components/statistics/index'
import './App.css'

import {
  makeStyles,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from '@material-ui/core'

// import { Graphics } from 'pixi.js'
import {
  Container,
  Sprite,
  Stage,
  TilingSprite,
  blendMode,
} from 'react-pixi-fiber'
import * as PIXI from 'pixi.js'

import { useSelector, useDispatch } from 'react-redux'

const useStyles = makeStyles({
  root: {
    maxWidth: 800,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  media: {
    width: '100%',
    height: 600,
  },
})

const useStylesCard = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})

function App() {
  return (
    <Router>
      <Nav />
      <div className='container'>
        <Header />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' component={About} />
          <Route path='/characters' exact component={Characters} />
          <Route
            path='/CharacterSpecification/:id'
            component={CharacterSpecification}
          />
          <Route path='/HandleReducer' component={HandleReducer} />
          <Route path='/Statistic' component={Statistic} />
        </Switch>
      </div>
    </Router>
  )
}

const OPTIONS = {
  height: 600,
  width: 800,
}

const bgTexture = PIXI.Texture.from('main.jpg')
const overlayTexture = PIXI.Texture.from('overlay.png')
const displacementTexture = PIXI.Texture.from('main.jpg')

const Home = () => {
  const classes = useStyles()
  const classes2 = useStylesCard()

  const bgTextureSprite = useRef()
  const displacementSprite = useRef()
  const overlaySprite = useRef()
  const [filters, setFilters] = useState({})

  useEffect(() => {
    const move = () => {
      overlaySprite.current.tilePosition.x += 1
      overlaySprite.current.tilePosition.y += 1
    }
    setFilters(new PIXI.filters.DisplacementFilter(bgTexture))

    PIXI.Ticker.shared.add(move, this)

    return () => {
      PIXI.Ticker.shared.remove(move, this)
    }
  }, [])

  return (
    <div>
      <Card className={classes.root}>
        <CardActionArea>
          <Stage options={OPTIONS}>
            <Container filters={filters}>
              <Sprite
                texture={bgTexture}
                ref={bgTextureSprite}
                width={800}
                height={600}
              />
              <TilingSprite
                ref={overlaySprite}
                texture={overlayTexture}
                width={800}
                height={600}
                alpha={0.5}
              />
            </Container>
          </Stage>

          <CardContent>
            <Typography gutterBottom variant='h5' component='h2'>
              Rick and Morty App
            </Typography>
            <Typography variant='body2' color='textSecondary' component='span'>
              <Card className={classes2.root}>
                <CardContent>
                  <Typography
                    className={classes2.title}
                    color='textSecondary'
                    gutterBottom
                  >
                    👁️ Usage: 🔥Notice that App is not in the final stadium yet.
                  </Typography>

                  <Typography className={classes2.pos} color='textSecondary'>
                    <p> Simple App with Rick and Morty Characters.</p>
                    <br />
                    <p>
                      {' '}
                      Character bar Displays Characters. Plus there is a search
                      bar.
                    </p>
                    <br />
                    <p>
                      {' '}
                      Clicking on the star element You enter the counter. You
                      can check what card were clicked in Statistic bar below
                      the current stats. You can also check all users "clicks"
                      below the All user stats
                    </p>
                    <br />
                    <p>
                      Notice: Favourites not working properly. There are buttons
                      "kliknij wyslij fetchem" if You click that on Current
                      stats it will restart stats in DB from Your current
                      session.
                    </p>
                  </Typography>
                </CardContent>
              </Card>
            </Typography>
          </CardContent>
        </CardActionArea>
        {/* <CardActions>
        <Button size='small' color='primary'>
          Share
        </Button>
        <Button size='small' color='primary'>
          Learn More
        </Button>
      </CardActions> */}
      </Card>
    </div>
  )
}

export default App
