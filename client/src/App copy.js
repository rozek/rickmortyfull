import React, { useEffect, useState, useRef } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import ReactDOM from 'react-dom'
import Nav from './components/navigation/Nav'
import Header from './components/ui/Header'
import About from './components/about/About'
import Characters from './components/characters/Characters'
import CharacterSpecification from './components/characters/CharacterSpecification'
import HandleReducer from './components/tools/HandleReducer'
// import Statistic from './components/statistics/Statistic'
import Statistic from './components/statistics/index'
import './App.css'

import {
  makeStyles,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from '@material-ui/core'

// import { Graphics } from 'pixi.js'
import {
  Container,
  Sprite,
  Stage,
  TilingSprite,
  blendMode,
} from 'react-pixi-fiber'
import * as PIXI from 'pixi.js'

// import mainPic from './img/main.jpg'

import { useSelector, useDispatch } from 'react-redux'

const useStyles = makeStyles({
  root: {
    maxWidth: 1245,
  },
  media: {
    width: 1000,
    height: 600,
  },
})

function App() {
  return (
    <Router>
      <Nav />
      <div className='container'>
        <Header />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' component={About} />
          <Route path='/characters' exact component={Characters} />
          <Route
            path='/CharacterSpecification/:id'
            component={CharacterSpecification}
          />
          <Route path='/HandleReducer' component={HandleReducer} />
          <Route path='/Statistic' component={Statistic} />
        </Switch>
        {/* <Link to='/Statistic' component={Statistic}> I will navigate You to Statistic mate</Link> */}
        {/* <LocationDisplay /> */}
      </div>
    </Router>
  )
}

const OPTIONS = {
  height: 600,
  width: 800,
}

const bgTexture = PIXI.Texture.from('main.jpg')
const displacementTexture = PIXI.Texture.from('disp-map.jpg')

const Home = () => {
  const classes = useStyles()

  const bgTextureSprite = useRef()
  const displacementSprite = useRef()
  const [filters, setFilters] = useState({})

  useEffect(() => {
    const move = () => {
      let min = Math.ceil(-2)
      let max = Math.floor(2)

      displacementSprite.current.tilePosition.x +=
        Math.floor(Math.random() * (max - min + 1)) + min
      displacementSprite.current.tilePosition.y +=
        Math.floor(Math.random() * (max - min + 1)) + min
    }
    setFilters(new PIXI.filters.DisplacementFilter(displacementTexture))

    PIXI.Ticker.shared.add(move, this)

    return () => {
      PIXI.Ticker.shared.remove(move, this)
    }
  }, [])

  return (
    <div>
      <Card className={classes.root}>
        <CardActionArea>
          <Stage options={OPTIONS}>
            <Container filters={filters}>
              <Sprite
                texture={bgTexture}
                ref={bgTextureSprite}
                width={800}
                height={600}
              />
              <TilingSprite
                ref={displacementSprite}
                texture={displacementTexture}
                width={800}
                height={600}
                alpha={0.5}
                blendMode={PIXI.BLEND_MODES.MULTIPLY}
              />
            </Container>
          </Stage>

          <CardContent>
            <Typography gutterBottom variant='h5' component='h2'>
              Rick and Morty App
            </Typography>
            <Typography variant='body2' color='textSecondary' component='p'>
              👁️ Usage:
            </Typography>
          </CardContent>
        </CardActionArea>
        {/* <CardActions>
        <Button size='small' color='primary'>
          Share
        </Button>
        <Button size='small' color='primary'>
          Learn More
        </Button>
      </CardActions> */}
      </Card>
    </div>
  )
}

export default App
