    const initialFavInfo = {
        listName: '',
        favIDs: []
    }

const isFavouriteRedurec = (state = initialFavInfo , action) => {
    let favState =  [...new Set([...state.favIDs, action.favID])]
    let favStateRmv= favState.filter(e =>  e !== action.favID)
    switch(action.type) {
        case 'ADDFAV':
            return {
                ...state, favIDs: favState
            }
        case 'RMVFAV':
            return {
                ...state, favIDs: favStateRmv
            }
        default:
            return state
    }
}

export default isFavouriteRedurec;