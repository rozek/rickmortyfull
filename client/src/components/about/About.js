import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}))

function About() {
  const classes = useStyles()
  return (
    <>
      <p style={{ paddingRight: '5px' }}> Contact authot </p>
      <Typography className={classes.root}>
        <Link href='https://michal.email'> Author</Link>
      </Typography>
    </>
  )
}

export default About
