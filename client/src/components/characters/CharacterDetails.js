import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import FavoriteIcon from '@material-ui/icons/Favorite'
import VisibilityIcon from '@material-ui/icons/Visibility'

import useFavsToLocal from '../hooks/useFavsToLocal'
import useAddFAV from '../hooks/useAddFAV'

import Special from './special'

import StarBorderTwoToneIcon from '@material-ui/icons/StarBorderTwoTone'

import { useSelector, useDispatch } from 'react-redux'

import { increment } from '../../actions'

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    // borderRadius: `${50}px`,
    background: '#fff',
    boxShadow: `${30}px ${30}px ${57}px #1f2427, 
             ${-30}px ${-30}px -${57}px #293035`,
  },
  media: {
    height: 240,
  },
  actionPanel: {
    backgroundColor: 'rgba(0,0,0,0.85)',
    color: 'rgba(30,140,280,1)',
    // color: '#fff'
  },
  clicked: {
    color: '#fff',
  },
})

let cardSetStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  // alignItems: 'center',
  flexDirection: 'column',
}

const CharacterDetails = ({ item, views, viewReqDB, views2 }) => {
  const savedValue = JSON.parse(localStorage.getItem('ID'))
  const favourites = useSelector((state) => state.isFavouriteRedurec)
  let favIDs = favourites.favIDs

  const [isFavourite, setIsFavourite] = useState(0)
  let [isViewed, setIsViewed] = useState(0)
  let [isViewedCounter, setIsViewedCounter] = useState(0)
  let [addFav, setAddFav] = useFavsToLocal('ID', [])
  let [fireAddFav, setFireAddFav] = useState(0)

  function fireFavs(param) {
    // setAddFav(favIDs)
    // console.log(favIDs)
    // setAddFav({ favIDs })
  }

  // useEffect(() => {
  //   console.log(favIDs)
  //   // setAddFav()
  //   console.log(addFav)
  // }, [addFav])

  let callbackFunction = function (text) {
    console.log({ text })
    setAddFav(text)
  }

  // console.log(favIDs)

  function doSomethingWithCallback(callback) {
    console.log(favIDs)
    callback(favIDs)
  }

  useEffect(() => {
    isFavourite % 2
      ? dispatch({ type: 'ADDFAV', favID: `${item.id}` })
      : dispatch({ type: 'RMVFAV', favID: `${item.id}` })
  }, [isFavourite])

  useEffect(() => {
    if (isViewedCounter > 0) {
      // isViewed = isViewed
      dispatch({ type: 'VIEWED', count: isViewed, id: `${item.id}` })
      console.log(' XXXXXXXXXX isViewed= ', isViewed, 'id= ', item.id)
    } else {
      dispatch({ type: 'VIEWED', count: isViewed, id: [] })
      console.log('NIE WYSZEDL')
    }
    // : console.log('')
    // } else { dispatch({ type: "VIEWED", count: isViewed, id: `${item.id}` })}
  }, [isViewed])

  const favourite = useSelector((state) => state.isFavouriteRedurec)

  const dispatch = useDispatch()

  const classes = useStyles()

  // Take views for display Accual State Views
  const countViews = () => {
    for (const [key, value] of Object.entries(views)) {
      if (`${item.id}` === key) {
        console.log(value)
        return value
      }
    }
  }

  return (
    <Card
      className={`${classes.root} flip-in-ver-right singleCard-${item.id} single-card`}
      style={cardSetStyle}
    >
      <Link to={`/characterspecification/${item.id}`}>
        <div>
          <CardActionArea>
            <CardMedia
              className={`${classes.media} single-card__picture`}
              image={item.image}
              alt={item.name}
            />
            <CardContent>
              <Typography gutterBottom variant='h5' component='h2'>
                {item.name}
              </Typography>
              <Typography variant='body2' color='textSecondary' component='p'>
                {item.species}
                {item.status}
              </Typography>
            </CardContent>
          </CardActionArea>
        </div>
      </Link>
      <CardActions className={classes.actionPanel}>
        <Button
          className={classes.actionPanel}
          onClick={() => dispatch(increment())}
          size='large'
          color='primary'
        >
          {countViews()}+
        </Button>

        <IconButton
          aria-label='add to favorites'
          className={classes.actionPanel}
          // onClick={() => {setIsFavourite(isFavourite + 1); IDsHandler(favIDs) }}
          onClick={() => {
            setIsFavourite(isFavourite + 1)
            // fireFavs(callbackFunction)
            // doSomethingWithCallback(callbackFunction)
            // setFireAddFav(1)
            // this.setAddFav({ favIDs }, () => {
            //   // console.log(this.state.updated);
            // })
          }}
        >
          <VisibilityIcon /> {favourite.favIDs}
        </IconButton>

        <Button size='small' color='primary'>
          <StarBorderTwoToneIcon
            className={classes.clicked}
            onClick={() => {
              setIsViewed(isViewed + 1)
              setIsViewedCounter(isViewedCounter + 1)
              viewReqDB(views2)
            }}
            // onClick={() => {setIsViewed(isViewed +1); callB(item); viewReqDB()}}
          />
        </Button>
      </CardActions>
    </Card>
  )
}

export default CharacterDetails
