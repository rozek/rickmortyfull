import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import CharacterDetails from './CharacterDetails'
import Search from '../ui/Search'
import GetCardStats from '../tools/getCardStats'

import { useSelector } from 'react-redux'
import HandleTest from '../tools/HandleTest'
import Spinner from '../ui/Spinner'

import './Characters.css'

const Characters = () => {
  const [items, setItems] = useState([])
  const [query, setQuery] = useState('')
  const [views, setViews] = useState({})
  const [isLoading, setIsLoading] = useState(true)

  const localURLs = 'https://graforickandmorty.herokuapp.com/cardStats'
  const URLs = [`https://rickandmortyapi.com/api/character/?name=${query}`]

  const viewedState = useSelector((state) => state.countView)
  const viewed = viewedState.count

  ////////////       [] z kliknietym parametrem
  let viewCounterResults = HandleTest(viewed)
  // const viewCounterResults = lastClickedElementDouble()

  console.log(viewCounterResults)

  console.log('views', views)

  const sumObjectsByKey = (...objs) => {
    const x = objs.reduce((a, b) => {
      console.log('>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<')
      let firstRun = true
      for (const k in b) {
        if (b.hasOwnProperty(k)) {
          if (firstRun) {
            a[k] = (a[k] || 0) + b[k]
            firstRun = false
          } else {
            a[k] = (a[k] || 0) + b[k]
          }
        }
      }
      return a
    }, {})

    return x
  }

  //-----------------------------------------------------------

  let combinedViews = sumObjectsByKey(views, viewCounterResults)
  const title = Object.keys(combinedViews)
  const description = Object.values(combinedViews)
  // console.log("sumObjectsByKey -> viewCounterResults", viewCounterResults)
  // console.log("combinedViews", combinedViews)
  // console.log("DESCRiption", description)
  // DESCRIPTION OK
  //---------------------------------------------------------------

  let sendPost = (titleSend, descriptionSend) => {
    fetchGo(titleSend, descriptionSend)
    // console.log(combinedViews);
  }
  console.log('_________________________________________')

  const fetchGo = (title, description) => {
    // const viewedUP = viewedState.count
    // console.log("viewedUP", viewedState)
    // console.log('%cDESCRIPTION in Fetch()', 'background: #222; color: #dd3a11', description);
    // console.log('%cTITLE in Fetch()', 'background: #222; color: #dd3a11', title);

    fetch('https://graforickandmorty.herokuapp.com/cardStats', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: title,
        description: description,
      }),
    })
      .then((res) => {
        return res.json()
      })
      .catch((error) => console.log(error))
  }

  ///////////////////// FETCH CARDS FROM API
  useEffect(() => {
    const fetchItems = async () => {
      console.time('⏱')
      for (const url of URLs) {
        const res = await fetch(url)
        const items = await res.json()
        setItems(items.results)
      }
      console.timeEnd('⏱')
    }
    fetchItems()

    const cardViews = async () => {
      ///Fetchowanie
      const data = await GetCardStats(localURLs)
      let keys = data[0].title
      let values = data[0].description
      //// Merge 2 Arrays => Object
      const cardViewsState = {}
      keys.forEach((key, i) => (cardViewsState[key] = values[i]))
      console.log(cardViewsState)

      setViews(cardViewsState)
      setIsLoading(false)
    }

    cardViews()
  }, [query])

  const favourites = useSelector((state) => state.isFavouriteRedurec)

  return isLoading ? (
    <Spinner />
  ) : (
    <div className='containerCards'>
      <Search getQuery={(q) => setQuery(q)} />
      <section className='cards'>
        {
          //  check items isn't null/undefined before rendering   ( items && items.map())
          items &&
            items.map((item) => (
              <CharacterDetails
                key={item.id}
                item={item}
                views={combinedViews}
                views2={views}
                viewReqDB={() => sendPost(title, description)}
              ></CharacterDetails>
            ))
        }
      </section>
    </div>
  )
}

export default Characters
