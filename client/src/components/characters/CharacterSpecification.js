import React, {useState, useEffect } from 'react'
import {Link} from 'react-router-dom'

const CharacterSpecification = ({ match }) => {


    const [items, setItems] = useState({
        location: {}
    })
const URLs = [
    `https://rickandmortyapi.com/api/character/${match.params.id}`,
]
useEffect(() => {
    console.log(match);
const fetchItems = async () => {
    
        for (const url of URLs) {
            const res = await fetch(url)
            const items = await res.json()
            setItems(items)  
            console.log(items);
        } 
    }
    fetchItems()
}, [])

    return (
        <div>
            <h1>{items.name}</h1>
            <img src={items.image}></img>
    <h4>{items.location.name}</h4>
        </div>
    )
}

export default CharacterSpecification
