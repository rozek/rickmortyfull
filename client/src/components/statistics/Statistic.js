import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import HandleReducer from '../tools/HandleReducer'
import HandleTest from '../tools/HandleTest'
import { Line } from 'react-chartjs-2'
import { GridList, Button } from '@material-ui/core'
import './statistics.css'
import { useLocation } from 'react-router'
import Media from 'react-media'

const Statistic = () => {
  const location = useLocation()
  console.log('Statistic -> location', location.pathname)

  function getStyles() {
    let statStyle = {}
    if (location.pathname == '/Statistic') {
      document.body.className = 'statistics'

      let statStyle = {
        background: '#f1f1f1',
        color: '#000',
        height: '300px',
        width: '500px',
        padding: '10px',
        margin: '10px',
        borderRadius: '15px',
      }
      return statStyle
    }
  }
  function getStyles2() {
    let statStyle = {}
    if (location.pathname == '/Statistic') {
      document.body.className = 'statistics'
      let statStyle = {
        background: '#f1f1f1',
        color: '#000',
        height: '300px',
        width: '300px',
        padding: '10px',
        margin: '10px',
        borderRadius: '15px',
      }
      return statStyle
    }
  }

  let width = window.innerWidth

  const mediaMatch = window.matchMedia('(min-width: 500)')

  const viewedState = useSelector((state) => state.countView)
  // console.log("Statistic -> viewedState", viewedState)
  const viewed = viewedState.count
  console.log('Statistic -> viewed', viewedState.count)

  const [chartData, setChartData] = useState({})

  const chart = (param) => {
    setChartData({
      // labels: ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'],
      // labels: param.count ,
      labels: witchNumbs,
      datasets: [
        {
          label: 'Click!/s',
          data: howMany,
          backgroundColor: ['rgba(50, 50, 170, 0.8)'],
          borderWidth: 4,
        },
      ],
    })
  }
  useEffect(() => {
    chart(viewedState)
  }, [])

  async function sendPost() {
    await fetch('http://localhost:5000/cardStats', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: witchNumbs,
        description: howMany,
      }),
    })
      .then((res) => {
        return res.json()
      })
      .then((data) => console.log(data))
      .catch((error) => console.log(error))
  }

  const viewCounterResults = HandleTest(viewed)
  // console.log("Statistic -> viewCounterResults", viewCounterResults)

  const witchNumbs = Object.keys(viewCounterResults)
  const howMany = Object.values(viewCounterResults)
  console.log('Statistic -> howMany', howMany)

  return (
    <div style={width < 600 ? getStyles2() : getStyles()}>
      <Line
        data={chartData}
        options={{
          responsive: true,
          title: { text: 'View Counter', display: true },
          scales: {
            yAxes: [
              {
                ticks: {
                  autoSkip: true,
                  maxTicksLimit: 10,
                  beginAtZero: true,
                },
                gridLines: {
                  display: true,
                },
              },
            ],
            xAxes: [
              {
                gridLines: {
                  display: false,
                },
              },
            ],
          },
        }}
      />
      <Button onClick={() => sendPost(viewedState)}>
        {' '}
        Kliknij wyslij fetchem
      </Button>
    </div>
  )
}

export default Statistic
