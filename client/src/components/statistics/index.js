import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Statistic from './Statistic'
import UsersChart from './UsersChart'
import Favourites from './Favourites'

const useStyles = makeStyles((theme) => ({
  root: {
    color: '#fff',
    borderRadius: '5px',
  },
  heading: {
    color: '#d7d7d7',
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    textTransform: 'uppercase',
  },

  body: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    //   backgroundColor: '#000'
  },
  extras: {
    transition: 'all 2s',
  },
  accorionLine: {
    border: '0',
    height: '1px',
    backgroundImage:
      'linear-gradient(to right, rgba(0, 0, 0, 0), rgba(180, 180, 180, 0.45), rgba(0, 0, 0, 0))',
  },
}))

let toogleOff = {
  color: '#fff',
  borderRadius: '5px',
  display: 'flex',
  alignItems: 'center',
  height: '300px',
  width: '0px',
  display: 'none',
}
let toogleOn = {
  background: 'RGBA(18,21,40,0.4)',
  color: '#fff',
  // height: '100%',
  width: '100%',
  borderRadius: '5px',
  display: 'flex',
  alignItems: 'center',
  height: '300px',
  width: '400px',
  border: '1px solid rgba(0, 0, 0, 0.25)',
  borderColor:
    'rgba(18, 21, 52, 0.75) transparent transparent rgba(18, 21, 52, 0.75)',
}
let acordionStyle = {
  backgroundColor: '#1d1d1d',
  borderRadius: '5px',
  boxShadow: '2px 2px 2px #f0fafa',
  boxShadow: ' -2px -2px 2x #f3f3f3',
}

function Charts() {
  const [toogleStyle, setToogleStyle] = useState(true)
  const [toogleStyleAllStats, setToogleStyleAllStats] = useState(true)

  function fireToggle() {
    setToogleStyle(!toogleStyle)
  }
  function fireToggleAllStats() {
    setToogleStyleAllStats(!toogleStyleAllStats)
  }

  const classes = useStyles()

  return (
    <div className='stats'>
      <div className={classes.root}>
        <Accordion style={acordionStyle}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls='panel1a-content'
            id='panel1a-header'
          >
            <Typography component={'span'} className={classes.heading}>
              Current Stats
            </Typography>
          </AccordionSummary>
          <hr className={classes.accorionLine}></hr>
          <AccordionDetails>
            <div className='charts'>
              <Typography component={'span'} className={classes.body}>
                <Statistic />
                <div className='toogleStats' onClick={() => fireToggle()}>
                  <div className='circle'>
                    <i className='arrow right'></i>
                  </div>
                </div>
                <div
                  className='neumorfCard'
                  style={toogleStyle ? toogleOff : toogleOn}
                >
                  <div className='chartDetailsContainer'>
                    <div className='chartDetails'>
                      <div className='chartDetails-info'>
                        <h2>Current cards visited in this session 📈</h2>
                        <hr className={classes.accorionLine}></hr>
                      </div>
                    </div>
                  </div>
                </div>
              </Typography>
            </div>
          </AccordionDetails>
        </Accordion>

        <Accordion style={acordionStyle}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls='panel1a-content'
            id='panel1a-header'
          >
            <Typography component={'span'} className={classes.heading}>
              All Users Stats
            </Typography>
          </AccordionSummary>
          <hr className={classes.accorionLine}></hr>
          <AccordionDetails>
            <div className='charts'>
              <Typography component={'span'} className={classes.body}>
                <UsersChart />
                <div
                  className='toogleStats'
                  onClick={() => fireToggleAllStats()}
                >
                  <div className='circle'>
                    <i className='arrow right'></i>
                  </div>
                </div>
                <div
                  className='neumorfCard'
                  style={toogleStyleAllStats ? toogleOff : toogleOn}
                >
                  <div className='chartDetailsContainer'>
                    <div className='chartDetails'>
                      <div className='chartDetails-info'>
                        <h2>Current cards visited in this session 📈</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </Typography>
            </div>
          </AccordionDetails>
        </Accordion>

        <Accordion style={acordionStyle}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls='panel1a-content'
            id='panel1a-header'
          >
            <Typography component={'span'} className={classes.heading}>
              Favourites Cards
            </Typography>
          </AccordionSummary>
          <hr className={classes.accorionLine}></hr>
          <AccordionDetails>
            <div className='charts'>
              <Typography className={classes.body}>
                <Favourites />
                <div
                  className='toogleStats'
                  onClick={() => fireToggleAllStats()}
                >
                  <div className='circle'>
                    <i className='arrow right'></i>
                  </div>
                </div>
                <div
                  className='neumorfCard'
                  style={toogleStyleAllStats ? toogleOff : toogleOn}
                >
                  <div className='chartDetailsContainer'>
                    <div className='chartDetails'>
                      <div className='chartDetails-info'>
                        <h2>Current cards visited in this session 📈</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </Typography>
            </div>
          </AccordionDetails>
        </Accordion>
      </div>
    </div>
  )
}

export default Charts
