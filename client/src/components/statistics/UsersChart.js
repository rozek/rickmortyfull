import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import HandleReducer from '../tools/HandleReducer'
import HandleTest from '../tools/HandleTest'
import { Line } from 'react-chartjs-2'
import { GridList, Button } from '@material-ui/core'
import './statistics.css'
import { useLocation } from 'react-router'
import Media from 'react-media'
import GetCardStats from '../tools/getCardStats'

const UsersCharts = () => {
  const location = useLocation()
  console.log('Statistic -> location', location.pathname)

  const [fullStats, setFullStats] = useState({})

  function getStyles() {
    let statStyle = {}
    if (location.pathname == '/Statistic') {
      document.body.className = 'statistics'

      let statStyle = {
        background: '#f1f1f1',
        color: '#000',
        height: '300px',
        width: '500px',
        padding: '10px',
        margin: '10px',
        borderRadius: '15px',
        '@media(maxWidth: 550px)': {
          background: 'black',
        },
      }
      return statStyle
    }
  }
  function getStyles2() {
    let statStyle = {}
    if (location.pathname == '/Statistic') {
      document.body.className = 'statistics'
      let statStyle = {
        background: '#f1f1f1',
        color: '#000',
        height: '300px',
        width: '300px',
        padding: '10px',
        margin: '10px',
        borderRadius: '15px',
        '@media(maxWidth: 550px)': {
          background: 'black',
        },
      }
      return statStyle
    }
  }

  let width = window.innerWidth

  const mediaMatch = window.matchMedia('(min-width: 500)')

  useEffect(() => {
    const cardViews = async () => {
      ///Fetchowanie
      const data = await GetCardStats(
        'https://graforickandmorty.herokuapp.com/cardStats'
      )
      let keys = data[0].title
      let values = data[0].description
      //// Merge 2 Arrays => Object
      const cardViewsState = {}
      keys.forEach((key, i) => (cardViewsState[key] = values[i]))
      console.log(cardViewsState)

      setFullStats(cardViewsState)
    }

    cardViews()
  }, [])

  const title = Object.keys(fullStats)
  console.log('UsersCharts -> title', title)
  const description = Object.values(fullStats)
  console.log('UsersCharts -> description', description)

  console.log(fullStats)

  const viewedState = useSelector((state) => state.countView)
  // console.log("Statistic -> viewedState", viewedState)
  const viewed = viewedState.count
  console.log('Statistic -> viewed', viewedState.count)

  const [chartData, setChartData] = useState({})

  const chart = (param) => {
    setChartData({
      labels: title,
      datasets: [
        {
          label: 'Click!/s',
          data: description,
          backgroundColor: ['rgba(180, 50, 50, 0.8)'],
          borderWidth: 4,
        },
      ],
    })
  }
  useEffect(() => {
    chart(fullStats)
  }, [fullStats])

  async function sendPost() {
    await fetch('https://graforickandmorty.herokuapp.com/cardStats', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: witchNumbs,
        description: howMany,
      }),
    })
      .then((res) => {
        return res.json()
      })
      .then((data) => console.log(data))
      .catch((error) => console.log(error))
  }

  const viewCounterResults = HandleTest(viewed)
  // console.log("Statistic -> viewCounterResults", viewCounterResults)

  const witchNumbs = Object.keys(viewCounterResults)
  const howMany = Object.values(viewCounterResults)
  console.log('Statistic -> howMany', howMany)

  return (
    <div style={width < 600 ? getStyles2() : getStyles()}>
      <Line
        data={chartData}
        options={{
          responsive: true,
          title: { text: 'View Counter', display: true },
          scales: {
            yAxes: [
              {
                ticks: {
                  autoSkip: true,
                  maxTicksLimit: 10,
                  beginAtZero: true,
                },
                gridLines: {
                  display: true,
                },
              },
            ],
            xAxes: [
              {
                gridLines: {
                  display: false,
                },
              },
            ],
          },
        }}
      />
      <Button onClick={() => sendPost(viewedState)}>
        {' '}
        Kliknij wyslij fetchem
      </Button>
    </div>
  )
}

export default UsersCharts
