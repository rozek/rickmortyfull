import React from 'react'

const GetCardStats = async (url) => fetch(url)
  .then(response => response.json())

export default GetCardStats
