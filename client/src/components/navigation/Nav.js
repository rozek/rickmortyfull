import React from 'react'
import { Link } from 'react-router-dom'
import { useLocation } from 'react-router'

const Nav = () => {
  const navStyle = {
    color: '#fff',
    textTransform: 'uppercase',
  }
  const location = useLocation()
  console.log('Statistic -> location', location.pathname)

  if (location.pathname !== '/Statistic') {
    document.body.classList.remove('statistics')
  }

  return (
    <nav>
      <Link style={navStyle} to='/'>
        <h3>Logo</h3>
      </Link>
      <ul className='nav-links'>
        <Link style={navStyle} to='/about'>
          <li>About</li>
        </Link>
        <Link style={navStyle} to='/Characters'>
          <li>Characters</li>
        </Link>
        <Link style={navStyle} to='/HandleReducer' disabled>
          <li>HandleReducer</li>
        </Link>
        <Link style={navStyle} to='/Statistic'>
          <li>Statistic</li>
        </Link>
      </ul>
    </nav>
  )
}

export default Nav
